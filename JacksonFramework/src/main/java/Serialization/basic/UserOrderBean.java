package Serialization.basic;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"sessionId", "sourceTopic", "source", "eventDate"})
public class UserOrderBean {

    @JsonProperty("Source topic")
    public String sourceTopic;
    public String eventSource;
    public String nextBillDate;
    @JsonProperty("Session id")
    public String sessionId;
    @JsonProperty("Event date")
    public String eventDate;
    @JsonProperty("Source")
    public String source;

    public UserOrderBean(String sourceTopic, String eventSource, String sessionId, String eventDate, String nextBillDate, String source) {
        this.sourceTopic = sourceTopic;
        this.eventSource = eventSource;
        this.sessionId = sessionId;
        this.eventDate = eventDate;
        this.nextBillDate = nextBillDate;
        this.source = source;
    }

}
