package Serialization.basic;

public class UserBean {
    public int id;
    public String firstName;

    public UserBean(int id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

//    public String getFirstName() {
//        return firstName;
//    }

    public String getVersion() {
        return "1.0";
    }
}
