package Serialization.basic;

import com.fasterxml.jackson.annotation.JsonGetter;

public class UserGetterBean {
    public int id;
    private String firstName;

    public UserGetterBean(int id, String firstName) {
        this.id = id;
        this.firstName = firstName;
    }

    @JsonGetter("name")
    public String getFirstName() {
        return firstName;
    }

    @JsonGetter("family")
    public String getLastName() {
        return "Jackson";
    }

    @JsonGetter()
    private String test() {
        return "abra";
    }
}
