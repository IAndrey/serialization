package Serialization.basic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserIgnoreBeanTest {
    @Test
    public void testUserIgnoreBean() throws JsonProcessingException {
        UserIgnoreBean userPropertyBean = new UserIgnoreBean(1, "Michael");
        String jsonResult = new ObjectMapper().writeValueAsString(userPropertyBean);
        System.out.println(jsonResult);

        assertEquals("{\"id\":1}", jsonResult);
    }
}