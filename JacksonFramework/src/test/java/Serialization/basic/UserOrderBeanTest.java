package Serialization.basic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

public class UserOrderBeanTest {
    @Test
    public void testUserOrderBean() throws JsonProcessingException {
        UserOrderBean userOrderBean = new UserOrderBean("1", "1", "1",
                "1", "1", "1");
        String result = new ObjectMapper().writeValueAsString(userOrderBean);
        System.out.println(result);
    }
}