package Serialization.basic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;


public class UserBeanTest {
    @Test
    public void testUserBeanModifiers() throws JsonProcessingException {
        UserBean user = new UserBean(1, "John");
        String resultJson = new ObjectMapper().writeValueAsString(user);
        System.out.println(resultJson);
        assertThat(resultJson, containsString("id"));
        assertThat(resultJson, containsString("firstName"));
    }
}