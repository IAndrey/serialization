package Serialization.basic;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class UserPropertyBeanTest {
    @Test
    public void testUserPropertyBean() throws JsonProcessingException {
        UserPropertyBean userPropertyBean = new UserPropertyBean(1, "Michael");
        String jsonResult = new  ObjectMapper().writeValueAsString(userPropertyBean);
        System.out.println(jsonResult);

        assertThat(jsonResult, containsString("id"));
        assertThat(jsonResult, containsString("name"));
    }
}