package Externalizable;


import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Login igor = new Login("IgorIvanovich", "Khoziain");
        Login renat = new Login("Renat", "2500RUB");
        //Записываем объекты в файл
        writeObj(igor, renat);
        //Считываем из файла объекты класса и выводим в консоль
        readObj();
    }

    private static void writeObj(Login... person) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("" +
                "Login.txt"));
        objectOutputStream.writeObject(person[0]);
        objectOutputStream.writeObject(person[1]);
        objectOutputStream.close();
    }

    private static void readObj() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream("Login.txt"));
        Login igorRestored = (Login) objectInputStream.readObject();
        Login renatRestored = (Login) objectInputStream.readObject();
        objectInputStream.close();
        System.out.println(igorRestored);
        System.out.println(renatRestored);
    }
}
