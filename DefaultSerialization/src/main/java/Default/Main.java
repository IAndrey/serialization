package Default;

import Default.impl.Home;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Home home = new Home("Vishnevaia 1");
        Person igor = new Person("Igor", 2, "Raphael", home);
        Person renat = new Person("Renat", 2, "Raphael", home);
        //Записываем объекты в файл
        writeObj(igor, renat);
        //Считываем из файла объекты класса и выводим в консоль
        readObj();
    }

    private static void writeObj(Person... person) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("" +
                "personOut.txt"));
        objectOutputStream.writeObject(person[0]);
        objectOutputStream.writeObject(person[1]);
        objectOutputStream.close();
    }

    private static void readObj() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream("personOut.txt"));
        Person igorRestored = (Person) objectInputStream.readObject();
        Person renatRestored = (Person) objectInputStream.readObject();
        objectInputStream.close();
        System.out.println(igorRestored);
        System.out.println(renatRestored);
    }
}
